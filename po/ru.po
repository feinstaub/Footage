# Russian translation for Footage.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the footage package.
# Сергей Ворон <voron@duck.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: footage\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-07 21:02-0500\n"
"PO-Revision-Date: 2023-10-28 11:10+0300\n"
"Last-Translator: Сергей Ворон <voron@duck.com>\n"
"Language-Team: \n"
"Language: ru_RU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);\n"
"X-Generator: Poedit 3.4\n"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:3
#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:5
#: data/resources/blueprints/window.blp:9
#: data/resources/blueprints/window.blp:42 src/main.rs:48
msgid "Footage"
msgstr "Footage"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:4
msgid "Video Editor"
msgstr "Редактор видео"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:5
msgid "Polish Your Videos"
msgstr "Улучшайте свои видео"

#: data/io.gitlab.adhami3310.Footage.desktop.in.in:11
msgid "video;edit;trim;crop;convert"
msgstr ""
"video;edit;trim;crop;convert;видео;редактировать;обрезать;вырезать;"
"конвертировать"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:6
msgid "Window width"
msgstr "Ширина окна"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:10
msgid "Window height"
msgstr "Высота окна"

#: data/io.gitlab.adhami3310.Footage.gschema.xml.in:14
msgid "Window maximized state"
msgstr "Окно развернуто"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:6
msgid "Khaleel Al-Adhami"
msgstr "Khaleel Al-Adhami"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:7
msgid "Polish your videos"
msgstr "Улучшайте свои видео"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:13
msgid ""
"Trim, flip, rotate and crop individual clips. Footage is a useful tool for "
"quickly editing short videos and screencasts. It's also capable of exporting "
"any video into a format of your choice."
msgstr ""
"Обрезка, переворот, вращение и кадрирование ваших клипов. Footage — полезный "
"инструмент для быстрого редактирования коротких видеороликов и скринкастов. "
"Он также может экспортировать любое видео в любой формат по вашему выбору."

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:28
msgid "video"
msgstr "видео"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:29
msgid "edit"
msgstr "редактировать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:30
msgid "trim"
msgstr "обрезать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:31
msgid "crop"
msgstr "вырезать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:32
msgid "convert"
msgstr "конвертировать"

#: data/io.gitlab.adhami3310.Footage.metainfo.xml.in.in:48
msgid "Editing screen with default options"
msgstr "Экран редактирования с параметрами по умолчанию"

#: data/resources/blueprints/help-overlay.blp:11
msgctxt "shortcut window"
msgid "General"
msgstr "Общие"

#: data/resources/blueprints/help-overlay.blp:14
msgctxt "shortcut window"
msgid "Open File"
msgstr "Открыть файл"

#: data/resources/blueprints/help-overlay.blp:19
msgctxt "shortcut window"
msgid "New Window"
msgstr "Новое окно"

#: data/resources/blueprints/help-overlay.blp:24
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Показать комбинации клавиш"

#: data/resources/blueprints/help-overlay.blp:29
msgctxt "shortcut window"
msgid "Quit"
msgstr "Выйти"

#: data/resources/blueprints/crop.blp:24
msgid "Top left crop corner"
msgstr "Верхний левый угол обрезки"

#: data/resources/blueprints/crop.blp:34
msgid "Top right crop corner"
msgstr "Верхний правый угол обрезки"

#: data/resources/blueprints/crop.blp:49
msgid "Bottom left crop corner"
msgstr "Нижний левый угол обрезки"

#: data/resources/blueprints/crop.blp:59
msgid "Bottom right crop corner"
msgstr "Нижний правый угол обрезки"

#: data/resources/blueprints/window.blp:26
msgid "Main Menu"
msgstr "Главное меню"

#: data/resources/blueprints/window.blp:43
msgid "Choose a video to edit"
msgstr "Выберите видео для редактирования"

#: data/resources/blueprints/window.blp:54
#: data/resources/blueprints/window.blp:379
msgid "Open Video…"
msgstr "Открыть видео..."

#: data/resources/blueprints/window.blp:76
msgid "Could not Load Video"
msgstr "Не удалось загрузить видео"

#: data/resources/blueprints/window.blp:77
msgid "This video could be corrupted or may use an unsupported file format."
msgstr ""
"Это видео может быть повреждено или использует неподдерживаемый формат файла."

#: data/resources/blueprints/window.blp:110
msgid "Rotate Left"
msgstr "Повернуть влево"

#: data/resources/blueprints/window.blp:115
msgid "Rotate Right"
msgstr "Повернуть вправо"

#: data/resources/blueprints/window.blp:120
msgid "Flip Horizontally"
msgstr "Перевернуть по горизонтали"

#: data/resources/blueprints/window.blp:125
msgid "Flip Vertically"
msgstr "Перевернуть по вертикали"

#: data/resources/blueprints/window.blp:130 src/window.rs:239
msgid "Disable Audio"
msgstr "Отключить звук"

#: data/resources/blueprints/window.blp:137
msgid "Resize"
msgstr "Изменить размер"

#: data/resources/blueprints/window.blp:142
#: data/resources/blueprints/window.blp:143
#: data/resources/blueprints/window.blp:151
#: data/resources/blueprints/window.blp:153
msgid "Width"
msgstr "Ширина"

#: data/resources/blueprints/window.blp:160
msgid "Preserve aspect ratio"
msgstr "Сохранить соотношение сторон"

#: data/resources/blueprints/window.blp:165
#: data/resources/blueprints/window.blp:166
#: data/resources/blueprints/window.blp:174
#: data/resources/blueprints/window.blp:175
msgid "Height"
msgstr "Высота"

#: data/resources/blueprints/window.blp:186
msgid "Choose between resizing with percentages or using exact pixels"
msgstr ""
"Выберите между изменением размера в процентах или использованием точных "
"пикселей"

#: data/resources/blueprints/window.blp:190
msgid "%"
msgstr "%"

#: data/resources/blueprints/window.blp:190
msgid "px"
msgstr "px"

#: data/resources/blueprints/window.blp:199
msgid "Framerate"
msgstr "Частота кадров"

#: data/resources/blueprints/window.blp:217
msgid "Container Format"
msgstr "Формат контейнера"

#: data/resources/blueprints/window.blp:223
msgid "Video Encoding"
msgstr "Видеокодек"

#: data/resources/blueprints/window.blp:229
msgid "Audio Encoding"
msgstr "Аудиокодек"

#: data/resources/blueprints/window.blp:240
msgid "Save Video"
msgstr "Сохранить видео"

#: data/resources/blueprints/window.blp:268
msgid "Video Exported"
msgstr "Видео экспортировано"

#: data/resources/blueprints/window.blp:278
msgid "View Result"
msgstr "Показать результат"

#: data/resources/blueprints/window.blp:290
msgid "Finish"
msgstr "Готово"

#: data/resources/blueprints/window.blp:312
msgid "Exporting Video Unsuccessful"
msgstr "Не удалось экспортировать видео"

#: data/resources/blueprints/window.blp:321
msgid "Retry"
msgstr "Повторить"

#: data/resources/blueprints/window.blp:341
msgid "Rendering…"
msgstr "Рендеринг…"

#: data/resources/blueprints/window.blp:343
msgid "This may take a while"
msgstr "Это может занять некоторое время"

#: data/resources/blueprints/window.blp:358 src/window.rs:493 src/window.rs:519
msgid "_Cancel"
msgstr "_Отменить"

#: data/resources/blueprints/window.blp:384
msgid "New Window"
msgstr "Новое окно"

#: data/resources/blueprints/window.blp:391
msgid "Keyboard Shortcuts"
msgstr "Комбинации клавиш"

#: data/resources/blueprints/window.blp:396
msgid "About Footage"
msgstr "О приложении"

#: data/resources/blueprints/timeline.blp:18
msgid "Left trimming handle"
msgstr "Левый указатель обрезки"

#: data/resources/blueprints/timeline.blp:52
msgid "Right trimming handle"
msgstr "Правый указатель обрезки"

#: src/window.rs:235
msgid "Enable Audio"
msgstr "Включить звук"

#: src/window.rs:400
msgid "Pause"
msgstr "Приостановить"

#: src/window.rs:403
msgid "Play"
msgstr "Воспроизвести"

#: src/window.rs:489 src/window.rs:514
msgid "Stop rendering?"
msgstr "Остановить рендеринг?"

#: src/window.rs:490 src/window.rs:515
msgid "You will lose all progress."
msgstr "Вы потеряете весь прогресс."

#: src/window.rs:494 src/window.rs:519
msgid "_Stop"
msgstr "_Остановить"

#: src/window.rs:541
msgid "Video Files"
msgstr "Видеофайлы"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/window.rs:787
msgid "translator-credits"
msgstr ""
"Сергей Ворон <voron@duck.com>\n"
"Aleksandr Melman <Alexmelman88@gmail.com>"

#: src/profiles.rs:78
msgid "Recommended (WEBM, AV1, Opus)"
msgstr "Рекомендуется (WEBM, AV1, Opus)"

#: src/profiles.rs:79
msgid "Keep as-is"
msgstr "Оставить как есть"

#~ msgid "Open File…"
#~ msgstr "Открыть файл…"

#~ msgid ""
#~ "This minor release introduces a fix for X11 and additional translation "
#~ "support."
#~ msgstr ""
#~ "В этом выпуске представлено исправление для X11 и дополнительная "
#~ "поддержка перевода."

#~ msgid ""
#~ "This minor release introduces a couple of bug fixes and enhances keyboard "
#~ "accessibility."
#~ msgstr ""
#~ "В этом минорном выпуске исправлено несколько ошибок и улучшены "
#~ "возможности клавиатуры."

#~ msgid "Initial version."
#~ msgstr "Первоначальная версия."
