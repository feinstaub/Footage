using Gtk 4.0;
using Adw 1;

template $AppWindow : Adw.ApplicationWindow {
  default-width: 800;
  default-height: 700;
  width-request: 300;
  height-request: 300;
  title: _("Footage");

  Adw.ToolbarView {
    [top]
    Adw.HeaderBar headerbar {
      Button back_edit {
        icon-name: 'go-previous-symbolic';
        tooltip-text: _('Edit Same Video');
        visible: false;
        styles ['flat']
      }


      [end]
      MenuButton {
        icon-name: "open-menu-symbolic";
        menu-model: primary_menu;
        tooltip-text: _("Main Menu");
        primary: true;
      }

      styles ["flat"]
    }

    content:
    Stack stack {
      transition-type: crossfade;

      StackPage {
        name: "welcome";
        child: 
        Adw.StatusPage {
          icon-name: "io.gitlab.adhami3310.Footage";
          title: _("Footage");
          description: _("Choose a video to edit");
          hexpand: true;
          vexpand: true;
          child: 
          Box {
            orientation: vertical;
            spacing: 12;

            Button open_video_button {
              valign: center;
              halign: center;
              label: _("Open Video…");
              action-name: "win.open";
              use-underline: true;

              styles [
                "suggested-action",
                "pill",
              ]
            }
          }

          ;
        }

        ;
      }

      StackPage {
        name: "invalid";
        child:
        Adw.StatusPage invalid_image {
          icon-name: "image-missing-symbolic";
          title: _("Could not Load Video");
          description: _("This video could be corrupted or may use an unsupported file format.");
          hexpand: true;
          vexpand: true;
        }

        ;
      }


      StackPage {
        name: "editing";
        child: 
        Adw.PreferencesPage {
          Adw.PreferencesGroup {
            $VideoPreview video_preview {}
          }
          Adw.PreferencesGroup {
            Box {
              spacing: 12;
              Button play_pause {
                icon-name: 'play-symbolic';
                tooltip-text: 'Play';
              }
              $Timeline timeline {}
            }
          }
          Adw.PreferencesGroup {
            Box {
              halign: center;
              spacing: 12;
              Button rotate_left_button {
                halign: center;
                icon-name: 'object-rotate-left-symbolic';
                tooltip-text: _("Rotate Left");
              }
              Button rotate_right_button {
                halign: center;
                icon-name: 'object-rotate-right-symbolic';
                tooltip-text: _("Rotate Right");
              }
              Button horizontal_flip_button {
                halign: center;
                icon-name: 'horizontal-flip-symbolic';
                tooltip-text: _("Flip Horizontally");
              }
              Button vertical_flip_button {
                halign: center;
                icon-name: 'vertical-flip-symbolic';
                tooltip-text: _("Flip Vertically");
              }
              ToggleButton audio_button {
                halign: center;
                icon-name: 'audio-volume-high-symbolic';
                tooltip-text: _("Disable Audio");
              }
            }
          }

          Adw.PreferencesGroup {
            Adw.ActionRow resize_amount_row {
              title: _("Resize");
              Box {
                spacing: 5;
                Entry resize_width_value {
                  valign: center;
                  placeholder-text: _("Width");
                  tooltip-text: _("Width");
                  input-purpose: digits;
                  max-width-chars: 6;
                  visible: false;
                  hexpand: false;
                }
                Entry resize_scale_width_value {
                  valign: center;
                  placeholder-text: _("Width");
                  input-purpose: digits;
                  tooltip-text: _("Width");
                  max-width-chars: 6;
                  hexpand: false;
                }
                Image link_axis {
                  valign: center;
                  icon-name: 'chain-link-symbolic';
                  tooltip-text: _("Preserve aspect ratio");
                  styles ['lock']
                }
                Entry resize_height_value {
                  valign: center;
                  placeholder-text: _("Height");
                  tooltip-text: _("Height");
                  input-purpose: digits;
                  max-width-chars: 6;
                  visible: false;
                  hexpand: false;
                }
                Entry resize_scale_height_value {
                  valign: center;
                  placeholder-text: _("Height");
                  tooltip-text: _("Height");
                  input-purpose: digits;
                  max-width-chars: 6;
                  hexpand: false;
                }
              }
              Adw.Bin {
                Box {
                  DropDown resize_type {
                    valign: center;
                    accessibility {
                      label: _("Choose between resizing with percentages or using exact pixels");
                    }
                    model:
                    StringList resize_types {
                      strings [_("%"),_("px")]
                    }
                    ;
                    styles ["flat", "unit-drop"]
                  }
                }
              }
            }
            Adw.SpinRow framerate_row {
              title: _("Framerate");
              adjustment: Adjustment {
                lower: 1;
                step-increment: 1;
                upper: 480;
              }

              ;
              value: 30;
            }
          }

          Adw.PreferencesGroup {
            Adw.ComboRow container_row {
              title: _("Container Format");
              use-subtitle: true;
              model: StringList {};
              styles ["property"]
            }
            Adw.ComboRow video_encoding {
              title: _("Video Encoding");
              use-subtitle: true;
              model: StringList {};
              styles ["property"]
            }
            Adw.ComboRow audio_encoding {
              title: _("Audio Encoding");
              use-subtitle: true;
              model: StringList {};
              styles ["property"]
            }
          }

          Adw.PreferencesGroup {
            Box {
              halign: center;
              Button save_button {
                label: _("Save Video");
                styles ["suggested-action", "pill"]
              }
            }
          }
        }

        ;
      }

      StackPage {
        name: "loading";
        child: 
        Spinner spinner {
          valign: center;
          halign: center;
          height-request: 28;
          width-request: 28;
        }

        ;
      }

      StackPage {
        name: "success";
        child: 
        Adw.StatusPage success_status {
          icon-name: "check-round-outline-symbolic";
          title: _("Video Exported");
          child: 
          Box {
            orientation: vertical;
            halign: center;
            spacing: 12;

            Button open_result {
              valign: center;
              halign: center;
              label: _("View Result");
              use-underline: true;
              width-request: 175;

              styles [
                "pill",
              ]
            }

            Button done_button {
              valign: center;
              halign: center;
              label: _("Finish");
              use-underline: true;
              width-request: 175;

              styles [
                "suggested-action",
                "pill",
              ]
            }
          }

          ;
        }

        ;
      }

      StackPage {
        name: "failure";
        child: 
        Adw.StatusPage {
          icon-name: "error-symbolic";
          title: _("Exporting Video Unsuccessful");
          child: 
          Box {
            orientation: vertical;
            spacing: 12;

            Button try_again_button {
              valign: center;
              halign: center;
              label: _("Retry");
              use-underline: true;

              styles [
                "suggested-action",
                "pill",
              ]
            }
          }

          ;
        }

        ;
      }

      StackPage {
        name: "exporting";
        child:
        Adw.StatusPage exporting_page {
          title: _("Rendering");
          icon-name: "camera-video-symbolic";
          description: _("This may take a while");

          Box {
            orientation: vertical;
            spacing: 36;

            ProgressBar progress_bar {
              valign: center;
              halign: center;
              show-text: true;
            }

            Button cancel_button {
              valign: center;
              halign: center;
              label: _("_Cancel");
              use-underline: true;

              styles [
                "pill",
              ]
            }
          }
        }

        ;
      }
    }

    ;
  }
}

menu primary_menu {
  section {
    item {
      label: _("Open Video…");
      action: "win.open";
    }

    item {
      label: _("New Window");
      action: "app.new-window";
    }
  }

  section {
    item {
      label: _("Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("About Footage");
      action: "win.about";
    }
  }
}
